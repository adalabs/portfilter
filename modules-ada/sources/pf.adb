--  with Ada.Strings.Fixed,
--       Ada.Text_IO;

package body PF
with SPARK_Mode
is
   User_Ranges  : array (Positive range 1 .. 100) of User_Port_Range_Type;

   Lowest_Port  : Port_Type := No_Port; -- Shall be after known ports end range
   Highest_Port : Port_Type := No_Port; -- Shall be before ephemeral port begin range

   procedure Initialize
   is
   begin
      Load_Rules;
   end Initialize;

   procedure Finalize
   is
   begin
      null;
   end Finalize;

   procedure Load_Rules
   is
      --  pragma Annotate (GNATprove, Might_Not_Return, Load_Rules);
      --  Rule_File_Name : constant String := "/etc/portfilter/portfilter.rules";
      --  Handler        :          Ada.Text_IO.File_Type;
      First_Line : Boolean := True;
   begin
      null;
      --  Ada.Text_IO.Open (File => Handler,
      --                    Mode => Ada.Text_IO.In_File,
      --                    Name => Rule_File_Name);
      --
      --  while not Ada.Text_IO.End_Of_File (Handler) loop
      --     declare
      --        Line   : constant String := Ada.Text_IO.Get_Line (Handler);
      --        Column :          Natural;
      --        Dash   :          Natural;
      --     begin
      --        Column := Ada.Strings.Fixed.Index (Source  => Line,
      --                                           Pattern => ":");
      --
      --        Dash := Ada.Strings.Fixed.Index (Source  => Line,
      --                                         Pattern => "-");
      --
      --        if Column /= 0 and then Dash /= 0 then
      --           declare
      --              User_Id    : User_Id_Type;
      --              Port_Range : Port_Range_Type;
      --           begin
      --              User_Id                := User_Id_Type'Value (Line (Line'First     .. Column - 1));
      --              Port_Range.Begin_Range := Port_Type   'Value (Line (Column     + 1 .. Dash   - 1));
      --              Port_Range.End_Range   := Port_Type   'Value (Line (Dash       + 1 .. Line'Last ));
      --
      --              if First_Line then
      --                 Lowest_Port  := Port_Range.Begin_Range;
      --                 Highest_Port := Port_Range.End_Range;
      --                 First_Line   := False;
      --              else
      --                 if Port_Range.Begin_Range < Lowest_Port then
      --                    Lowest_Port  := Port_Range.Begin_Range;
      --                 end if;
      --                 if Port_Range.End_Range > Highest_Port then
      --                    Highest_Port  := Port_Range.End_Range;
      --                 end if;
      --              end if;
      --
      --              User_Ranges_Containers.Insert (User_Ranges, User_Id, Port_Range);
      --           end;
      --        end if;
      --     end;
      --  end loop;
      --
      --  Ada.Text_IO.Close (Handler);

      User_Ranges (1) := (User_Id => 1_000,
                          Port_Range => (Begin_Range => 8_080,
                                         End_Range   => 8_090));
      User_Ranges (2) := (User_Id => 0,
                          Port_Range => (Begin_Range => 9_000,
                                         End_Range   => 9_010));
      for User_Port_Range of User_Ranges loop
         if User_Port_Range.User_Id /= No_User then
            if First_Line then
               Lowest_Port  := User_Port_Range.Port_Range.Begin_Range;
               Highest_Port := User_Port_Range.Port_Range.End_Range;
               First_Line   := False;
            else
               if User_Port_Range.Port_Range.Begin_Range < Lowest_Port then
                  Lowest_Port  := User_Port_Range.Port_Range.Begin_Range;
               end if;
               if User_Port_Range.Port_Range.End_Range > Highest_Port then
                  Highest_Port  := User_Port_Range.Port_Range.End_Range;
               end if;
            end if;
         end if;
      end loop;

   end Load_Rules;

   function Is_Port_Authorized (User_Id : User_Id_Type;
                                Port    : Port_Type) return PF_Boolean
   is
   begin
      if Lowest_Port = No_Port and Highest_Port = No_Port then
         Kernel_Info (210);
         return True;
      else
         if Port < Lowest_Port or Port > Highest_Port then
            Kernel_Info (220);
            return True;
         else
            for User_Port_Range of User_Ranges loop
               if User_Port_Range.User_Id = User_Id then
                  if Port >= User_Port_Range.Port_Range.Begin_Range and then Port <= User_Port_Range.Port_Range.End_Range then
                     Kernel_Info (200);
                     return True;
                  else
                     Kernel_Info (403);
                     return False;
                  end if;
               end if;
            end loop;
            Kernel_Info (400);
            return False;
         end if;
      end if;
   end Is_Port_Authorized;

end PF;
