
package PF
with SPARK_Mode
is
   type PF_Boolean is (False, True) with Convention => C;
   for PF_Boolean use (False => 0, True => 1);

   type User_Id_Type is new Integer with Convention => C;

   No_User : constant User_Id_Type := -1;

   type Port_Type is new Integer with Convention => C;

   No_Port : constant Port_Type := -1;

   type Port_Range_Type is record
      Begin_Range : Port_Type;
      End_Range   : Port_Type;
   end record;

   type User_Port_Range_Type is record
      User_Id    : User_Id_Type := No_User;
      Port_Range : Port_Range_Type;
   end record;

   procedure Load_Rules;

   procedure Initialize;
   procedure Finalize;

   function Is_Port_Authorized (User_Id : User_Id_Type;
                                Port    : Port_Type) return PF_Boolean;

   pragma Export (Convention    => C,
                  Entity        => Initialize,
                  External_Name => "portfilter_initialize");
   pragma Export (Convention    => C,
                  Entity        => Finalize,
                  External_Name => "portfilter_finalize");
   pragma Export (Convention    => C,
                  Entity        => Load_Rules,
                  External_Name => "portfilter_load_rules");
   pragma Export (Convention    => C,
                  Entity        => Is_Port_Authorized,
                  External_Name => "portfilter_is_port_authorized");


   type Kernel_Info_Code_Type is new Natural with Convention => C;
   procedure Kernel_Info (Code : Kernel_Info_Code_Type);
   pragma Import (Convention    => C,
                  Entity        => Kernel_Info,
                  External_Name => "kernel_info");

end PF;
