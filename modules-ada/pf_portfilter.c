/*
 * parlty inspired on https://stackoverflow.com/questions/2103315/linux-kernel-system-call-hooking-example
 * mapping of bind system call parameters and pt_regs is available on https://syscalls64.paolostivanin.com/
 *
 * Kernel >= 5.10
 * Usage:
 *
 *     sudo cat /proc/kallsyms | grep sys_call_table
 *     sudo insmod portfilter.ko sys_call_table_addr=0x<address_here>
 */
#include <linux/module.h> /* Needed by all modules */
#include <linux/kernel.h> /* Needed for KERN_INFO */
#include <linux/init.h>   /* Needed for the macros */
#include <linux/cred.h>

#include <asm/special_insns.h>   // {read,write}_cr0()
#include <asm/processor-flags.h> // X86_CR0_WP
#include <asm/unistd.h>          // __NR_*

#include <net/sock.h>

MODULE_LICENSE ("GPL");
MODULE_AUTHOR ("David Sauvage - AdaLabs Ltd");
MODULE_DESCRIPTION ("PortFilter Module - bind system call hijacking");
MODULE_VERSION("0.0.2");

extern void portfilter_initialize (void);
extern void portfilter_finalize (void);
extern int portfilter_is_port_authorized (int user_id, int port);

typedef long (*sys_call_ptr_t)(const struct pt_regs *);

static sys_call_ptr_t *real_sys_call_table;
static sys_call_ptr_t original_bind;

static unsigned long sys_call_table_addr;
module_param(sys_call_table_addr, ulong, 0);
MODULE_PARM_DESC(sys_call_table_addr, "Address of sys_call_table");

// Since Linux v5.3 [native_]write_cr0 won't change "sensitive" CR0 bits, need
// to re-implement this ourselves.
static void write_cr0_unsafe(unsigned long val)
{
  asm volatile("mov %0,%%cr0": "+r" (val) : : "memory");
}

void kernel_info (int code){
       pr_info ("pf code %d", code);
}

// mapping of bind system call parameters and pt_regs is available on https://syscalls64.paolostivanin.com/
//
// int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
//              rdi                            rsi             rdx
//
static long portfilter_bind (const struct pt_regs *regs)
{
  struct sockaddr_in *sin;
  uint16_t port;
  int sockfd = regs->di;
  const struct sockaddr *addr = (struct sockaddr *)regs->si;
  //int addrlen = regs->dx;
  switch(addr->sa_family){
    case AF_INET:
      sin = (struct sockaddr_in *)addr;
      port = htons (sin->sin_port);
      if ( portfilter_is_port_authorized (get_current_user()->uid.val, port) == 1 ) {
        pr_info ("AF_INET [granted] %pI4:%d (user %u fd %d)\n", &sin->sin_addr, port, get_current_user()->uid.val, sockfd);
        return original_bind(regs);
      } else {
        pr_info ("AF_INET [denied] %pI4:%d (user %u fd %d)\n", &sin->sin_addr, port, get_current_user()->uid.val, sockfd);
        return -1;
      }
      break;
    default:
      pr_info ("unmanaged sockfd addr->sa_family %hu (user %u fd %d)\n", addr->sa_family, get_current_user()->uid.val, sockfd);
      return original_bind(regs);
      break;
  }
}

static int __init portfilter_begin(void)
{
  unsigned long old_cr0;

  pr_info("module loading ...\n");
  if ( sys_call_table_addr == 0 ) {
    pr_info("module loading failure, sys_call_table_addr parameter unset\n");
    return 1;
  }
  real_sys_call_table = (typeof(real_sys_call_table))sys_call_table_addr;

  // Temporarily disable CR0 WP to be able to write to read-only pages
  old_cr0 = read_cr0();
  write_cr0_unsafe(old_cr0 & ~(X86_CR0_WP));

  // Overwrite syscall and save original to be restored later
  original_bind = real_sys_call_table[__NR_bind];
  real_sys_call_table[__NR_bind] = portfilter_bind;

  // Restore CR0 WP
  write_cr0_unsafe(old_cr0);
  portfilter_initialize();
  pr_info("module loaded\n");
  return 0;
}

static void __exit portfilter_end(void)
{
  unsigned long old_cr0;

  old_cr0 = read_cr0();
  write_cr0_unsafe(old_cr0 & ~(X86_CR0_WP));

  // Restore original syscall
  real_sys_call_table[__NR_bind] = original_bind;

  write_cr0_unsafe(old_cr0);
  portfilter_finalize();
  pr_info ("module unloaded\n");
}

module_init(portfilter_begin);
module_exit(portfilter_end);
