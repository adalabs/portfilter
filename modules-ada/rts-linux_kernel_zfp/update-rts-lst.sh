#!/bin/bash

REF=$ADALABS_ROOT_PATH/rts-adalabs/gnat-fsf-13/adainclude/
TARGET=adainclude

function update-gpr-lst () {
	local filename=$1
	rm -rf $TARGET/$filename
	touch $TARGET/$filename
	for f in `cat $REF/$filename`;
	do
		if [ -f $TARGET/$f ]; then
			printf "$f\n" >> $TARGET/$filename;
		fi
	done
}
function update-gpr-c-lst () {
        local filename=$1
        for f in `grep "[c|h]$" $REF/$filename`;
        do
		printf "$f\n" >> $TARGET/$filename;
                if [ ! -f $TARGET/$f ]; then
			cp $REF/$f $TARGET/
                fi
        done
	printf "unwind-pe.h\n" >> $TARGET/$filename;
	if [ ! -f $TARGET/unwind-pe.h ]; then
                        cp $REF/unwind-pe.h $TARGET/
        fi
}
#update-gpr-lst "libgnat.lst"
#update-gpr-lst "libgnarl.lst"
#update-gpr-c-lst "libgnat.lst"
echo "not appropriate for minimalist Ada Run Time for Linux Kernel"
