# portfilter

## About

portfilter is a loadable kernel module to specify port filter rules, mainly to reserve a specific port range to a given user.
`/etc/portfilter/portfilter.rules` specification below reserves the port range from `8_080` to `8_090` to user id `1000` (user `love`) and port range from `9_000` to `9_010` to user id `0` (user `root`).

```
1000:8_080 .. 8_090
0:9_000 .. 9_010
```

To avoid any conflicts, the portfilter specified port range values shall be outside;

  - The GNU/Linux well-known port range (i.e outside `0 .. 1_023` port range), and 
  - The GNU/Linux ephemeral port range (i.e outside `32_768 .. 60_999`). The following command returns the ephemeral port range `# sysctl net.ipv4.ip_local_port_range`)

In a nutshell, the `bind` system call will be hijacked by the portfilter loadable kernel module, in order to check if the requested port is in the reserved port range of the user. If so, the original `bind` system call is processed, and if not an error is returned to the user-space. From the specification example above;

  - When the bind port is outside of the  `/etc/portfilter/portfilter.rules` lowest and highest ports (i.e lowest port `8_080` and highest port `9_010`), portfilter does not interfere and the original bind system call is called directly
  - The user `love` would be able to successfully process a bind when the port is in the port range `8_080 .. 8_090`
  - The user `love` will not be able to successfully process a bind when the port is in the port range `8_091 .. 9_010`

## Roadmap

1. [x] Be able to load/unload the portfilter loadable kernel module
2. [x] Be able to hijack the `bind` system call
3. [x] Be able to run Ada in the loadable module kernel-space
4. [x] ZFP Ada run time for linux kernel-space
   1. [x] Minimalist version `ada.ads`, `a-unccon.ads`, `interfac.ads`, `s-stoele.adb`, `s-stoele.ads`, `s-unstyp.ads`, `system.ads`
   2. [ ] Add support to
       1. [ ] Basic `Ada.Text_IO` for file open/read/close operations
       2. [ ] `Interfaces.C` pointers
       3. [ ] Memory allocator 
       4. [ ] Memory deallocator
       5. [ ] Secondary stack
       6. [ ] Local exception handler
       7. [ ] Last chance exception handler
       8. [ ] Big numbers
   3. [ ] Support other features nice to get in the Linux kernel-space
5. [ ] Handle porfilter rules specification `/etc/portfilter/portfilter.rules`
   1. [ ] Load them in the C side, and pass them to the Ada side as a parameter
   2. [ ] Load them directly from the Ada side
6. [ ] Evaluate if the portfilter loadable kernel module can be handled by the alire infrastructure
6. [ ] portfilter SPARK/Ada implementation, use of SPARK Libraries
7. [ ] portfilter formal verification and proof of absence of runtime errors
8. [ ] Handle unconstraint number of portfilter specification rules

## Prerequisites

```
# apt install git make linux-headers-amd64
```
An up-to-date GNAT FSF Ada toolchain is also required.
It is recommended for developers to test the kernel module on a GNU/Linux Debian 12 virtual machine, and not their main workstation.

## Build
 
```
$ git clone git@gitlab.com:adalabs/portfilter.git
$ make build
```

The `make build` command will build;

  - The minimalist Ada run time (see `modules-ada/rts-linux_kernel_zfp/libada.gpr`)
  - The portfilter Ada component (see `modules-ada/portfilter.gpr`)
  - The portfilter loadable kernel module (see see `modules-ada/pf_portfilter.c`)

## Insert the portfilter module

In another terminal, run `journalctl -k -f`

```
# make insmod
```

The `make insmod` command will retrieve the `sys_call_table` address using `/proc/kallsyms` and load the loadable kernel module with the `sys_call_table_addr` parameter set accordingly


##  Remove the portfilter module

In another terminal, run `journalctl -k -f`

```
# make rmmod
```

## Test

### Case where a bind request is granted by portfilter

Launch a simple web server running on `$ADDR:$PORT`, where `$PORT` is in the portfilter reserve port range of the user, and call it

```
$ export ADDR=0.0.0.0 ; export PORT=8081
$ nc -q 1 -l -p $PORT -c 'echo "HTTP/1.1 200 OK\r\nContent-Length: 33\r\n\r\n $(date)"' -s $ADDR
```

On another shell, Launch the web client request, which is completed successfully

```
$ wget "localhost:$PORT" -O /tmp/wget.result
$ cat /tmp/wget.result
 Mon Mar 11 01:35:54 PM +04 2024
```

the kernel log `journalctl -k -f` are as follow, portfilter has granted the bind.

```
Mar 11 13:33:56 adalabs-poc kernel: AF_INET [granted] 0.0.0.0:8081 (user 1000 fd 3)
```

### Case where a bind request is denied by portfilter

Try to Launch a simple web server running on `$ADDR:$PORT`, where `$PORT` is not in the portfilter reserve port range of the user, and call it

```
$ export ADDR=0.0.0.0 ; export PORT=8091
$ nc -q 1 -l -p $PORT -c 'echo "HTTP/1.1 200 OK\r\nContent-Length: 33\r\n\r\n $(date)"' -s $ADDR
Can't grab 0.0.0.0:8091 with bind : Operation not permitted
```
The web server can not be launched as the bind request has been denied.

The kernel log `journalctl -k -f` are as follow, portfilter has denied the bind

```
Mar 11 13:40:08 adalabs-poc kernel: AF_INET [denied] 0.0.0.0:8091 (user 1000 fd 3)
```

## Documentations

  - https://docs.kernel.org/kbuild/makefiles.html
  - https://learn.adacore.com/ 
  - https://syscalls64.paolostivanin.com/

### Minimalist ZFP Ada run time for linux kernel-space

The Minimalist ZFP Ada run time for linux kernel-space is organized as below;

```mermaid
graph TB
      subgraph "libgnat (CONFIG=Minimalist)"
            libgnat_Ada[Ada \n 1]
            libgnat_Ada__Unchecked_Conversion[Ada.Unchecked_Conversion \n 1]
            libgnat_Interfaces[Interfaces \n 1]
            libgnat_System[System \n 1]
            libgnat_System__Storage_Elements[System.Storage_Elements \n 2]
            libgnat_System__Unsigned_Types[System.Unsigned_Types \n 1]
      end
libgnat_System__Storage_Elements --> libgnat_Ada__Unchecked_Conversion
```
(generated with `CONFIG=Minimalist adadesigner -F mermaid -l 3 -c modules-ada/rts-linux_kernel_zfp/adainclude/libada.gpr`)

  * `ada.ads`
  * `a-unccon.ads`
  * `interfac.ads`
  * `s-stoele.adb`
  * `s-stoele.ads`
  * `s-unstyp.ads`
  * `system.ads`

Global restrictions are;

  * `pragma Discard_Names;`
  * `pragma Restrictions (No_Enumeration_Maps);`
  * `pragma Normalize_Scalars;`
  * `pragma Restrictions (No_Exception_Propagation);`
  * `pragma Restrictions (No_Finalization);`
  * `pragma Restrictions (No_Tasking);`
  * `pragma Restrictions (No_Protected_Types);`
  * `pragma Restrictions (No_Delay);`
  * `pragma Restrictions (No_Recursion);`
  * `pragma Restrictions (No_Dispatch);`
  * `pragma Restrictions (No_Implicit_Dynamic_Code);`
  * `pragma Restrictions (No_Exception_Handlers);`
  * `pragma Restrictions (No_Secondary_Stack);`
  * `pragma Restrictions (No_Allocators);`



### Minimalist + C Pointers and Secondary Stack ZFP Ada run time for linux kernel-space

The Minimalist + C Pointers and Secondary Stack ZFP Ada run time for linux kernel-space is organized as below;

```mermaid
graph TB
      subgraph "libgnat (CONFIG=C_Pointers)"
            libgnat_Ada[Ada \n 1]
            libgnat_Ada__Unchecked_Conversion[Ada.Unchecked_Conversion \n 1]
            libgnat_Ada__Unchecked_Deallocation[Ada.Unchecked_Deallocation \n 1]
            libgnat_Interfaces[Interfaces \n 1]
            libgnat_Interfaces__C[Interfaces.C \n 2]
            libgnat_Interfaces__C__Pointers[Interfaces.C.Pointers \n 2]
            libgnat_System[System \n 1]
            libgnat_System__Parameters[System.Parameters \n 2]
            libgnat_System__Secondary_Stack[System.Secondary_Stack \n 2]
            libgnat_System__Soft_Links[System.Soft_Links \n 2]
            libgnat_System__Soft_Links__Initialize[System.Soft_Links.Initialize \n 2]
            libgnat_System__Stack_Checking[System.Stack_Checking \n 2]
            libgnat_System__Storage_Elements[System.Storage_Elements \n 2]
            libgnat_System__Unsigned_Types[System.Unsigned_Types \n 1]
      end
libgnat_Interfaces__C --> libgnat_System__Parameters
libgnat_Interfaces__C__Pointers --> libgnat_Ada__Unchecked_Conversion
libgnat_Interfaces__C__Pointers --> libgnat_System
libgnat_Interfaces__C__Pointers --> libgnat_System__Parameters
libgnat_System__Secondary_Stack --> libgnat_Ada__Unchecked_Conversion
libgnat_System__Secondary_Stack --> libgnat_Ada__Unchecked_Deallocation
libgnat_System__Secondary_Stack --> libgnat_System__Parameters
libgnat_System__Secondary_Stack --> libgnat_System__Soft_Links
libgnat_System__Secondary_Stack --> libgnat_System__Storage_Elements
libgnat_System__Soft_Links --> libgnat_System__Parameters
libgnat_System__Soft_Links --> libgnat_System__Secondary_Stack
libgnat_System__Soft_Links --> libgnat_System__Soft_Links__Initialize
libgnat_System__Soft_Links --> libgnat_System__Stack_Checking
libgnat_System__Soft_Links__Initialize --> libgnat_System__Secondary_Stack
libgnat_System__Stack_Checking --> libgnat_System__Storage_Elements
libgnat_System__Storage_Elements --> libgnat_Ada__Unchecked_Conversion
```
(generated with `CONFIG=C_Pointers adadesigner -F mermaid -l 3 -c modules-ada/rts-linux_kernel_zfp/adainclude/libada.gpr`)

  * `ada.ads`
  * `a-unccon.ads`
  * `interfac.ads`
  * `s-stoele.adb`
  * `s-stoele.ads`
  * `s-unstyp.ads`
  * `system.ads`
  * `c_pointers/i-c.ads`
  * `c_pointers/i-cpoint.ads`
  * `c_pointers/no_exception_handlers/i-c.adb`
  * `c_pointers/no_exception_handlers/i-cpoint.adb`
  * `c_pointers/s-parame.adb`
  * `c_pointers/s-parame.ads`
  * `secondary_stacks/a-uncdea.ads`
  * `secondary_stacks/no_exception_handlers/s-secsta.adb`
  * `secondary_stacks/no_exception_handlers/s-soflin.adb`
  * `secondary_stacks/no_exception_handlers/s-soflin.ads`
  * `secondary_stacks/s-secsta.ads`
  * `secondary_stacks/s-soliin.adb`
  * `secondary_stacks/s-soliin.ads`
  * `secondary_stacks/s-stache.adb`
  * `secondary_stacks/s-stache.ads`

Global restrictions are;

  * `pragma Discard_Names;`
  * `pragma Restrictions (No_Enumeration_Maps);`
  * `pragma Normalize_Scalars;`
  * `pragma Restrictions (No_Exception_Propagation);`
  * `pragma Restrictions (No_Finalization);`
  * `pragma Restrictions (No_Tasking);`
  * `pragma Restrictions (No_Protected_Types);`
  * `pragma Restrictions (No_Delay);`
  * `pragma Restrictions (No_Recursion);`
  * `pragma Restrictions (No_Dispatch);`
  * `pragma Restrictions (No_Implicit_Dynamic_Code);`

Lifted global restrictions are;

  * `No_Exception_Handlers`
  * `No_Secondary_Stack`
  * `No_Allocators`
  
Missing references defined in `kernel.c`;
  * `__gl_default_stack_size`
  * `__gnat_last_chance_handler`
  * `__gnat_free`
  * `__gnat_malloc`
  
